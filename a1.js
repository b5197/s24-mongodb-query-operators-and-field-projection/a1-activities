//1st
db.users.find({$or:[{fistName: {$regex: 'y', $options: '$i'}}, {lastName: {$regex: 'y', $options: '$i'}}]},{"_id":0,"email":1, "isAdmin": 1})

//2nd
db.users.find({$and:[{fistName: {$regex: 'e', $options: '$i'}}, {isAdmin: true}]},{"_id":0,"email":1, "isAdmin": 1})

//3rd
db.newProducts.find({$and:[{name: {$regex: 'x', $options: '$i'}}, {price: {$gte: 50000}}]})

//4th
db.newProducts.updateMany({price: {$lt: 2000}}, {$set: {isActive: false}})

//5th
db.newProducts.deleteMany({price: {$gt: 2000}})